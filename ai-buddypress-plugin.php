<?php
/*
Plugin Name: ai-buddypress-plugin
Version: 0.0.1
Plugin URI: none
Description: Filtra alcuni parametri per la privacy
Author: A/I
*/

require_once( dirname( __FILE__ ) . '/buddypress-ai-filter-functions.php' );
add_action( 'bp_has_activities', 'my_denied_activities', 10, 2 );

//Utilizza gli avatar di buddypress se presenti
function nfm_bp_avtar_upload_path_correct($path){
	if ( bp_core_is_multisite() ){
		$path = ABSPATH . get_blog_option( BP_ROOT_BLOG, 'upload_path' );
	}
	return $path;
}
add_filter('bp_core_avatar_upload_path', 'nfm_bp_avtar_upload_path_correct', 1);

function nfm_bp_avatar_upload_url_correct($url){
	if ( bp_core_is_multisite() ){
		#$url = get_blog_option( BP_ROOT_BLOG, 'siteurl' ) . "/wp-content/uploads";
		$url = site_url( 'wp-content/uploads' );
	}
	return $url;
}
add_filter('bp_core_avatar_url', 'nfm_bp_avatar_upload_url_correct', 1);


?>
